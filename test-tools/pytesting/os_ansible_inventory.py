#!/usr/bin/env python
################################################################################
# Dynamic inventory generation for Ansible
# Author lukas.pustina@codecentric.de
#
# This Python script generates a dynamic inventory based on OpenStack instances.
#
# The script is passed via -i <script name> to ansible-playbook. Ansible
# recognizes the execute bit of the file and executes the script. It then
# queries nova via the novaclient module and credentials passed via environment
# variables -- see below.
#
# The script iterates over all instances of the given tenant and checks if the
# instances' metadata have set keys OS_METADATA_KEY -- see below. These keys shall
# contain all Ansible host groups comma separated an instance shall be part of,
# e.g., u'ansible_host_groups': u'admin_v_infrastructure,apt_repos'.
# It is also possible to set Ansible host variables, e.g.,
# u'ansible_host_vars': u'dns_server_for_domains->domain1,domain2;key2->value2'
# Values with a comma will be transformed into a list.
#
# Metadata of an instance may be set during boot, e.g.,
# > nova boot --meta <key=value>
# , or to a running instance, e.g.,
# nova meta <instance name> set <key=value>
#
# *** Requirements ***
# * Python: novaclient module be installed which is part of the nova ubuntu
# package.
# * The environment variables OS_USERNAME, OS_PASSWORD, OS_TENANT_NAME,
# OS_AUTH_URL must be set according to nova.
#
# *** Short comings ***
# Currently, the name of the network is hardcoded in the global variable
# OS_NETWORK_NAME. This might be mitigated by another environment variable, but
# this might not interoperate well.
#
################################################################################

from __future__ import print_function
from novaclient import client as nova_client_lib
import os, sys, json
from os import environ as env

OS_METADATA_KEY = {
    'host_groups': 'ansible_host_groups',
    'host_vars': 'ansible_host_vars'
}

OS_NETWORK_NAME = 'FACTORY-dmz-gveics636k6s'

def main(args):
    credentials = getOsCredentialsFromEnvironment()

    nova_client = nova_client_lib.Client(
        credentials['VERSION'],
        credentials['USERNAME'],
        credentials['PASSWORD'],
        credentials['TENANT_NAME'],
        credentials['AUTH_URL'],
        service_type="compute",
        region_name=credentials['REGION']
    )

    inventory = {
        '_meta': {
            'hostvars': {}
        }
    }

    connection_infos = {
        'ansible_ssh_user': 'cloud',
        'ansible_ssh_private_key_file': '~/.ssh/amaury-ext-compute.pem'
    }

    for server in nova_client.servers.list():

        security_group_names = [sg.name for sg in server.list_security_group()]

        server_addresses = server.addresses.get(OS_NETWORK_NAME)

        has_floating = 'floating' in [addr['OS-EXT-IPS:type'] for addr in server_addresses]

        every_floating_addr = [ addr for addr in server_addresses if addr['OS-EXT-IPS:type'] == 'floating']
        every_floating_groups = []
        for floating_addr in every_floating_addr:
            floating_addr_ip = floating_addr['addr']
            addServerToHostGroup('entry_points', floating_addr_ip, inventory)
            every_floating_groups.append(floating_addr_ip.replace('.','_'))

            addServerHostVarsToHostVars(connection_infos, floating_addr_ip, inventory)

        every_fixed_addr = [ addr for addr in server_addresses if addr['OS-EXT-IPS:type'] == 'fixed']
        for fixed_addr in every_fixed_addr:
            fixed_addr_ip = fixed_addr['addr']

            for sg in security_group_names:
                addServerToHostGroup(sg, fixed_addr_ip, inventory)
            for fip in every_floating_groups:
                addServerToHostGroup(fip, fixed_addr_ip, inventory)

            addServerHostVarsToHostVars(connection_infos, fixed_addr_ip, inventory)

            if not every_floating_addr:
                addServerToHostGroup('private_servers', fixed_addr_ip, inventory)

        #host_vars = getAnsibleHostVarsFromServer(nova_client, server.id)
        #if host_vars:
        #    addServerHostVarsToHostVars(host_vars, addr_ip, inventory)

    dumpInventoryAsJson(inventory)


def getOsCredentialsFromEnvironment():
    credentials = {}
    try:
        credentials['VERSION'] = env['OS_COMPUTE_API_VERSION']
        credentials['USERNAME'] = env['OS_USERNAME']
        credentials['PASSWORD'] = env['OS_PASSWORD']
        credentials['TENANT_NAME'] = env['OS_TENANT_NAME']
        credentials['AUTH_URL'] = env['OS_AUTH_URL']
        credentials['REGION'] = env['OS_REGION_NAME']
    except KeyError as e:
        print("ERROR: environment variable %s is not defined" % e, file=sys.stderr)
        sys.exit(-1)

    return credentials


def getAnsibleHostGroupsFromServer(novaClient, serverId):
    metadata = getMetaDataFromServer(novaClient, serverId, OS_METADATA_KEY['host_groups'])
    if metadata:
        return metadata.split(',')
    else:
        return []


def getMetaDataFromServer(novaClient, serverId, key):
    return novaClient.servers.get(serverId).metadata.get(key, None)


def getAnsibleHostVarsFromServer(novaClient, serverId):
    metadata = getMetaDataFromServer(novaClient, serverId, OS_METADATA_KEY['host_vars'])
    if metadata:
        host_vars = {}
        for kv in metadata.split(';'):
            key, values = kv.split('->')
            values = values.split(',')
            host_vars[key] = values
        return host_vars
    else:
        return None


def find_floating_ip_from_server_for_network(server, network):
    for addr in server.addresses.get(network):
        if addr.get('OS-EXT-IPS:type') == 'floating':
            return addr['addr']
    return None


def addServerToHostGroup(group, floatingIp, inventory):
    host_group = inventory.get(group, {})

    hosts = host_group.get('hosts', [])

    hosts.append(floatingIp)

    host_group['hosts'] = hosts

    inventory[group] = host_group


def addServerHostVarsToHostVars(host_vars, floatingIp, inventory):
    inventory_host_vars = inventory['_meta']['hostvars'].get(floatingIp, {})
    inventory_host_vars.update(host_vars)
    inventory['_meta']['hostvars'][floatingIp] = inventory_host_vars


def dumpInventoryAsJson(inventory):
    print(json.dumps(inventory, indent=4))


if __name__ == "__main__":
    main(sys.argv)
